
resource "aws_instance" "ambiente-dev" {
  count = 1
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name = "terraform_key"
  security_groups = ["allow_ssh","allow_http","allow_egress"]
  user_data = file("script2.sh")

  tags = {
    Name = "ambiente-dev-${count.index}"
  }
}